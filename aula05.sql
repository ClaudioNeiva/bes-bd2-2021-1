-- Union e union all

alter table professor
	rename column nome_professor to nome;
alter table professor
	rename column matricula_professor to matricula;

alter table professor
	add column telefone char(11);
alter table aluno
	add column telefone char(11);

select *
from professor;

update professor
set	telefone = '7199887766'
where nome = 'Fernando';
update professor
set	telefone = '7163463555'
where nome = 'Osvaldo';
update professor
set	telefone = '7564645644'
where nome = 'Arnaldo';
update professor
set	telefone = '7398587575'
where nome = 'Claudio';
update professor
set	telefone = '71856565553'
where nome = 'Mario';

select *
from aluno;

update aluno
set	telefone = '2342342344'
where nome = 'Claudio Neiva';
update aluno
set	telefone = '2344443434'
where nome = 'Pedro';
update aluno
set	telefone = '3434444444'
where nome = 'Antonio';
update aluno
set	telefone = '3434222111'
where nome = 'Maria';
update aluno
set	telefone = '2233444555'
where nome = 'José Claudio';

update aluno
set	telefone = '2233444555',
	nome = 'Claudio'
where nome = 'José Claudio';

update aluno
set	telefone = '7398587575',
	nome = 'Claudio'
where nome = 'Antonio Claudio Neiva';

select 	nome, 
		telefone
from 	aluno
union
select 	nome, 
		telefone
from 	professor
order by 1 asc;

select 	'aluno' as tipo,
		nome, 
		telefone
from 	aluno
union
select 	'professor' as tipo,
		nome, 
		telefone
from 	professor
order by 1 asc;

select 	nome, 
		telefone
from 	aluno
union all
select 	nome, 
		telefone
from 	professor
order by 1 asc;

-- Subconsultas
select	*
from proposta_matricula;
select *
from aluno;
select *
from disciplina;
update aluno
set	nome = 'Joaquim'
where matricula = 7;

insert into proposta_matricula (matricula_aluno, ano, semestre, cod_disciplina, situacao)
values 
	((select matricula from aluno where nome = 'Pedro'),2021,1,'BES022','ABT'),
	((select matricula from aluno where nome = 'Pedro'),2021,1,'BES040','ABT'),
	((select matricula from aluno where nome = 'Maria'),2021,1,'BES022','ABT'),
	((select matricula from aluno where nome = 'Maria'),2021,1,'BES035','ABT'),
	((select matricula from aluno where nome = 'Joaquim'),2021,1,'BES008','CNF'),
	((select matricula from aluno where nome = 'Joaquim'),2021,1,'BES035','CCL'),
	((select matricula from aluno where nome = 'Joaquim'),2021,1,'BES022','ABT'),
	((select matricula from aluno where nome = 'Joaquim'),2021,1,'BES040','ABT');

select *
from proposta_matricula;

-- Quais alunos não fizeram nenhuma proposta de matrícula?
-- Resultado: nome e telefone do aluno
-- Ordenado por nome
-- Em outras palavras: quais regitros na tabela de alunos não possuem valor matricula 
-- na tabela de proposta_matricula (campo matricula_aluno).

-- NÃO faça assim:
select	a.nome, 
		a.telefone
from aluno a
left outer join proposta_matricula pm on (pm.matricula_aluno = a.matricula)
where pm.matricula_aluno is null;

-- Faça assim:
select	a.nome, 
		a.telefone
from 	aluno a
where	a.matricula not in (select	matricula_aluno
							from 	proposta_matricula);

-- Quais alunos não fizeram nenhuma proposta de matrícula?
-- Resultado: nome e telefone do aluno
select distinct 
		a.nome, 
		a.telefone
from aluno a
inner join proposta_matricula pm on (pm.matricula_aluno = a.matricula);

select	a.nome, 
		a.telefone
from 	aluno a
where	a.matricula in (select	matricula_aluno
						from 	proposta_matricula);

-- NÃO faça assim:?
select	a.nome, 
		a.telefone
from 	aluno a
where	exists (select	matricula_aluno
				from 	proposta_matricula pm
			   where	pm.matricula_aluno = a.matricula);


-- Views
alter table disciplina
	rename column codigo_disciplina to codigo;
alter table disciplina
	rename column nome_disciplina to nome;	

create or replace view v_proposta_matricula
as
	select	a.matricula as "matricula_aluno",
			a.nome as "nome_aluno",
			d.codigo as "codigo_disciplina",
			d.nome as "nome_disciplina",
			pm.ano,
			pm.semestre,
			pm.situacao as "situacao_proposta"
	from	proposta_matricula pm
	inner join aluno a on (a.matricula = pm.matricula_aluno)
	inner join disciplina d on (d.codigo = pm.cod_disciplina);

select	*
from	v_proposta_matricula;

select	vpm.*,
		p.nome as "nome_professor"
from	v_proposta_matricula vpm
inner join disciplina d on (d.codigo = vpm.codigo_disciplina)
inner join professor p on (p.matricula = d.matricula_professor)
where	situacao_proposta = 'ABT';

create or replace view v_proposta_matricula
as
	select	a.matricula as "matricula_aluno",
			a.nome as "nome_aluno",
			d.codigo as "codigo_disciplina",
			d.nome as "nome_disciplina",
			pm.ano,
			pm.semestre,
			pm.situacao as "situacao_proposta"
	from	aluno a 
	left outer join proposta_matricula pm on (a.matricula = pm.matricula_aluno)
	left join disciplina d on (d.codigo = pm.cod_disciplina);

select	*
from	v_proposta_matricula;

drop view if exists v_proposta_matricula;

create or replace view v_proposta_matricula
as
	select	a.matricula as "matricula_aluno",
			a.nome as "nome_aluno",
			d.codigo as "codigo_disciplina",
			d.nome as "nome_disciplina",
			pm.ano,
			pm.semestre,
			pm.situacao as "situacao_proposta",
			p.nome as "nome_professor"
	from	aluno a 
	left outer join proposta_matricula pm on (a.matricula = pm.matricula_aluno)
	left join disciplina d on (d.codigo = pm.cod_disciplina)
	left join professor p on (p.matricula = d.matricula_professor);

select	*
from	v_proposta_matricula;

-- Views materializadas duplicam do dado! 
-- Melhor performance e piora o espaço de armazenamento.
create materialized view vm_proposta_matricula
as
	select	a.matricula as "matricula_aluno",
			a.nome as "nome_aluno",
			d.codigo as "codigo_disciplina",
			d.nome as "nome_disciplina",
			pm.ano,
			pm.semestre,
			pm.situacao as "situacao_proposta",
			p.nome as "nome_professor"
	from	aluno a 
	left outer join proposta_matricula pm on (a.matricula = pm.matricula_aluno)
	left join disciplina d on (d.codigo = pm.cod_disciplina)
	left join professor p on (p.matricula = d.matricula_professor);

select	*
from	vm_proposta_matricula;

select	*
from	v_proposta_matricula;

update 	proposta_matricula
set		situacao = 'CCL'
where	matricula_aluno  = 4
and		cod_disciplina = 'BES022';

select	*
from	vm_proposta_matricula
order by 1 asc, 3 asc;

select	*
from	v_proposta_matricula
order by 1 asc, 3 asc;

refresh materialized view vm_proposta_matricula;

select	*
from	vm_proposta_matricula
order by 1 asc, 3 asc;

-- Isso era feito quando não existiam as views materizalizadas.
-- NÃO faça assim:
drop TABLE if exists t_proposta_matricula;
CREATE TABLE t_proposta_matricula 
AS 
	select	a.matricula as "matricula_aluno",
			a.nome as "nome_aluno",
			d.codigo as "codigo_disciplina",
			d.nome as "nome_disciplina",
			pm.ano,
			pm.semestre,
			pm.situacao as "situacao_proposta",
			p.nome as "nome_professor"
	from	aluno a 
	left outer join proposta_matricula pm on (a.matricula = pm.matricula_aluno)
	left join disciplina d on (d.codigo = pm.cod_disciplina)
	left join professor p on (p.matricula = d.matricula_professor);
	
select *
from t_proposta_matricula;

-- Usando arquivos no SSO como tabelas no SGBD:
CREATE EXTENSION file_fdw;
CREATE SERVER local_file FOREIGN DATA WRAPPER file_fdw;
CREATE FOREIGN TABLE words 
(word text NOT NULL)
SERVER local_file
OPTIONS (filename '/usr/share/dict/words');

select count(*)
from words;

select *
from words
where word ilike 'Zebr%';

CREATE MATERIALIZED VIEW vm_words 
AS 
	SELECT *
	FROM words;
	
select *
from vm_words
where word ilike 'Zebr%';

-- CENÁRIO PARA USO DE TRIGGERS

-- Sistema financeiro

-- Tabelas
-- conta: codigo, nome, tipo (R/D/A = Receita/Despesa/Ativo)
-- movimento: data, conta_origem, conta_destino, valor, historico

-- Exemplos de dados:
-- conta: 
-- (1, 'Água', 'D')
-- (2, 'Luz', 'D')
-- (3, 'Salário', 'R')
-- (4, 'Santander', 'A')
-- (5, 'Condomínio', 'D')
-- (6, 'Escola', 'D')
-- movimento:
-- ('2021-03-01', 3, 4, 5000.00, 'UCSal')
-- ('2021-03-03', 4, 1, 300.00, 'Embasa - Apt 1')
-- ('2021-03-03', 4, 5, 800.00, 'Condomínio - Apt 1')
-- ('2021-03-04', 4, 2, 400.00, 'Coelba')
-- ('2021-03-08', 4, 6, 1400.00, 'Escola de João')

-- Como saber o saldo no Santander? (posição inicial de um extrato, realizar um saque, etc)
-- Calculando a diferença entre as entradas e as saídas.
-- E se eu tiver 20 anos de conta? Quais impactos para essa estratégia de calcular a diferença?
-- Lembrando que para emissão de extrato, precisamos saber o saldo em uma data.
-- Extrato: saldo inicial, lista de movimentos, saldo final.

-- Solução interessante: manter uma tabela de saldos
-- saldo: data, conta (geralmente apenas contas de Ativo precisam ter o saldo calculado), valor

-- Exemplo de dados na conta de saldo:
-- ('2021-03-01', 4, 5000.00)
-- ('2021-03-03', 4, 3900.00)
-- ('2021-03-04', 4, 3500.00)
-- ('2021-03-08', 4, 2100.00)

-- Emitir o extrato de 05 a 10 de março de 2021
-- O saldo no dia 04/03/2021 e os movimentos entre 05 e 10/03/2021.

-- Dado derivado?
-- A manuntenção de dados derivados DEVE ser feita por TRIGGERS!

-- Triggers além de auxiliarem na manutenção de dados derivados, são utilizados para 
-- controle de restrições que não são cobertas pela constraint de check.







