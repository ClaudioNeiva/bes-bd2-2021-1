package br.ucsal.bes20211.bd2.aula12;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");

		try {

			EntityManager em = emf.createEntityManager();

			// Aluno alunoNovo = new Aluno(13, "Clara");
			//
			// em.getTransaction().begin();
			// em.persist(alunoNovo);
			// em.getTransaction().commit();

			// Aluno aluno = new Aluno(10, "Clara");
			//
			// em.getTransaction().begin();
			// aluno = em.merge(aluno);
			// em.getTransaction().commit();

			Aluno aluno = em.find(Aluno.class, 11);
			
			System.out.println(aluno);
			
			System.out.println("em.contains(aluno)=" + em.contains(aluno));

			em.getTransaction().begin();
			aluno.setNome("Joaldo");
			em.getTransaction().commit();

		} finally {

			emf.close();

		}

	}

}
