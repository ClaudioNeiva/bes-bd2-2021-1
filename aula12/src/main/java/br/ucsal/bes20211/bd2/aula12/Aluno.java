package br.ucsal.bes20211.bd2.aula12;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

import org.apache.log4j.Logger;

@Entity
public class Aluno {

	private static Logger logger = Logger.getLogger(Aluno.class);

	@Id
	private Integer matricula;

	private String nome;
	
	public Aluno() {
	}

	public Aluno(Integer matricula, String nome) {
		super();
		this.matricula = matricula;
		this.nome = nome;
	}

	@PrePersist
	public void prePersist() {
		logger.debug("Executei o método de prePersist no aluno de matrícula " + matricula);
	}

	@PostPersist
	public void postPersist() {
		logger.debug("Executei o método de postPersist no aluno de matrícula " + matricula);
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + "]" + super.toString();
	}

}
