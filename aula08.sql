select 	*
from 	item;

-- Alteração para suporte à auditoria de INSERT
alter table item
	add column usuario 	name ;
alter table item
	add column dthr_operacao timestamp ;
alter table item 
	add column operacao varchar(6);
-- Alteração, removendo o suporte à auditoria da tabela de INSERT, pois a auditoria vai migrar toda para a tabela de historico
alter table item
	drop column usuario;
alter table item
	drop column dthr_operacao;

-- Alteração para suporte à auditoria de DELETE
create table historico (
	id			serial		not null,
	usuario		name		not null,
	dthr		timestamp	not null,
	tabela		name		not null,
	descricao	text		not null,
	operacao	varchar(6)  not null,
	constraint pk_historico
		primary key (id));

create or replace function f_trg_ins_upd_del_auditoria()
returns trigger
as
$$
declare v_registro record;
begin
	if TG_OP IN ('DELETE') then
		v_registro := OLD;
	else
		v_registro := NEW;
	end if;
	
	insert into historico (usuario, dthr, tabela, descricao, operacao)
	values (CURRENT_USER, CURRENT_TIMESTAMP, TG_TABLE_NAME, v_registro, TG_OP);

	if TG_OP = 'DELETE' then
		return OLD;
	else
		return NEW;
	end if;
end;
$$
language PLPGSQL;

create or replace function f_trg_ins_upd_del_auditoria()
returns trigger
as
$$
begin
	if TG_OP = 'DELETE' then
		insert into historico (usuario, dthr, tabela, descricao, operacao)
		values (CURRENT_USER, CURRENT_TIMESTAMP, TG_TABLE_NAME, OLD, TG_OP);
		return OLD;
	else
		insert into historico (usuario, dthr, tabela, descricao, operacao)
		values (CURRENT_USER, CURRENT_TIMESTAMP, TG_TABLE_NAME, NEW, TG_OP);
		return NEW;
	end if;
end;
$$
language PLPGSQL;	
	
drop trigger if exists trg_ins_upd_del_auditoria on item;

create trigger trg_ins_upd_del_auditoria_item
before INSERT or UPDATE or DELETE
on item
for each row
execute procedure f_trg_ins_upd_del_auditoria();

create trigger trg_ins_upd_del_auditoria_aluno
before INSERT or UPDATE or DELETE
on aluno
for each row
execute procedure f_trg_ins_upd_del_auditoria();

-- Operações: INSERT, UPDATE e DELETE

-- 1. Quem fez a operação de INSERT e quando?
-- a) Acrescentar os campos de usuario, dthr_operacao
-- b) Criar uma tabela de auditoria: id, usuario, dthr_operacao, desc_reg. Ou uma tabela única para todas as auditorias:
-- incluir o campo tabela;

insert into item (nome) values ('prego');
insert into item (nome) values ('arruela');

a) atualizar os atributos usuario := CURRENT_USER e dthr_operacao := CURRENT_TIMESTAMP
b) item_historico (usuario, dthr_operacao, desc_reg) values (CURRENT_USER, CURRENT_TIMESTAMP, 'id:'||NEW.id||'nome:'||NEW.nome);

delete 
from item
where nome = 'Bobina';

select 	*
from 	historico;

-- 2. Quem fez a operação de DELETE, quando e qual dado foi excluído?
-- a) Utilizar o conceito de vigência. Fim de vigência indica quando o registro não é mais válido.
-- b) Tabela a parte para armazenar o estado anterior do registro.

-- c) vigência

-- 3. Quem fez a operação de UPDATE, quando fez e qual o dado antigo que foi substituido. 

update 	item
set		nome = 'Prego'
where	nome = 'prego';

select *
from item;

select 	*
from 	historico;

delete 	
from 	historico;

insert into item (nome)
values ('jaca1');

insert into item (nome)
values ('caju2');

update item
set	nome = 'caju1_v2'
where	nome = 'caju1';

select *
from item;

delete 
from	item
where	nome = 'caju1_v2';

select *
from aluno;

insert into aluno (nome, cpf, email, ano_nascimento, uf, telefone)
values ('Guilherme Tanajura','12312312','guilherme@ucsal.br',2002, 'BA', '123122233');

