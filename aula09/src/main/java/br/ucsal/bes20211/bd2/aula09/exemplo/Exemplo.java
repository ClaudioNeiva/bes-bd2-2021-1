package br.ucsal.bes20211.bd2.aula09.exemplo;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.persistence.CorrentistaDAO;
import br.ucsal.bes20211.bd2.aula09.persistence.DBUtil;

public class Exemplo {

	public static void main(String[] args) throws SQLException {
		try {
			DBUtil.connect("postgres", "abcd1234");
			abortarInsercao();
			listarCorrentistas();
		} finally {
			DBUtil.close();
		}
	}
	
	private static void abortarInsercao() throws SQLException {

		boolean autoCommit = DBUtil.getConnection().getAutoCommit();

		DBUtil.getConnection().setAutoCommit(false);

		// Entre o primeiro comando de um transação que altera a base 
		// e o comando que fecha a transação (commit, rollback)
		// NÃO deve haver dependência de uma interação do usuário.
		
		Correntista correntista1 = new Correntista("Renato", "457467567", 2001);
		CorrentistaDAO.persist(correntista1);
		Correntista correntista2 = new Correntista("Guilherme", "33423432", 2007);
		CorrentistaDAO.persist(correntista2);

		DBUtil.getConnection().rollback();
		// DBUtil.getConnection().commit();

		DBUtil.getConnection().setAutoCommit(autoCommit);

	}

	private static void atualizarCorrentista(List<Correntista> correntistas) throws SQLException {
		correntistas.get(3).setNome("Yla");
		CorrentistaDAO.merge(correntistas.get(3));
	}

	private static void criarCorrentista() throws SQLException {
		Correntista correntista1 = new Correntista("Antonio Claudio Neiva", "234234", 1975);
		CorrentistaDAO.persist(correntista1);
		System.out.println("Correntista1.id=" + correntista1.getId());
	}

	private static List<Correntista> listarCorrentistas() throws SQLException {
		List<Correntista> correntistas = CorrentistaDAO.findAll();
		System.out.println("Correntistas: ");
		correntistas.stream().forEach(System.out::println);
		return correntistas;
	}

}
