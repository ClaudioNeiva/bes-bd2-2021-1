-- Excluir todas as tabelas, para depois recriá-las
drop table if exists proposta_matricula cascade;
drop table if exists aluno cascade;
drop table if exists aluno_telefone cascade;
drop table if exists disciplina cascade;
drop table if exists professor cascade;

-- proposta_matricula (matricula_aluno Pk Fk, ano Pk, semestre Pk, cod_disciplina Pk, situacao)
create table proposta_matricula (
	matricula_aluno		integer			not null,
	ano					smallint		not null,
	semestre			smallint		not null,
	cod_disciplina		char(6)			not null,
	situacao			char(3)			not null,
	constraint pk_proposta_matricula
		primary key (matricula_aluno, ano, semestre, cod_disciplina),
	constraint ck_proposta_matricula_semestre
		check (semestre in (1,2)),
	constraint ck_proposta_matricula_situacao
		check (situacao in ('ABT','CNF','CCL')) -- ABT=aberta;CNF=confirmada;CCL=cancelada
);

-- aluno(matricula Pk, nome)
-- obs: um atributo foi acrescentado, para ilustrar o uso de restrições de unicidade
create table aluno (
	matricula			serial			not null,
	nome				varchar(40)		not null,
	cpf					char(11)		not null,
	constraint pk_aluno
		primary key (matricula),
	constraint un_aluno_cpf
		unique (cpf) 
);

-- aluno_telefone(matricula Pk Fk, telefone)
create table aluno_telefone (
	matricula			integer			not null,
	telefone			varchar(14)		not null,
	constraint pk_aluno_telefone
		primary key (matricula, telefone));

-- disciplina(codigo Pk, nome, matricula_professor Fk)
create table disciplina (
	codigo				char(6)			not null,
	nome				varchar(30)		not null,
	matricula_professor	integer			    null,
	constraint pk_disciplina
		primary key (codigo));		

-- professor(matricula Pk, nome)
create table professor (
	matricula			serial			not null,
	nome				varchar(40)		not null,
	constraint pk_professor
		primary key (matricula));

-- Criar as chaves estrageiras (foreign key, FK)
alter table proposta_matricula
	add	constraint fk_proposta_matricula_aluno
		foreign key (matricula_aluno)
		references aluno,
	add	constraint fk_proposta_matricula_disciplina
		foreign key (cod_disciplina)
		references disciplina;

alter table aluno_telefone
	add	constraint fk_aluno_telefone
		foreign key (matricula)
		references aluno
		on delete cascade
		on update cascade;

alter table disciplina
	add	constraint fk_disciplina
		foreign key (matricula_professor)
		references professor;
