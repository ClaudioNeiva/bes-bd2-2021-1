-- Constraint check;
create table proposta_matricula (
	matricula_aluno		integer			not null,
	ano					smallint		not null,
	semestre			smallint		not null,
	cod_disciplina		char(6)			not null,
	situacao			char(3)			not null,
	constraint pk_proposta_matricula
		primary key (matricula_aluno, ano, semestre, cod_disciplina),
	constraint ck_proposta_matricula_semestre
		check (semestre in (1,2)),
	constraint ck_proposta_matricula_situacao
		check (situacao in ('ABT','CNF','CCL')) -- ABT=aberta;CNF=confirmada;CCL=cancelada
);


-- Having

select *
from aluno
order by uf;

select	count(*) as "Qtd alunos",
		avg(extract(year from current_date) - ano_nascimento) 	as "A média das idades",
		uf
from	aluno
group by uf;

-- Em quais estados a idade média é superior à 30 anos?
-- Resposta: uf e a idade média

select	uf,
		avg(extract(year from current_date) - ano_nascimento) 	as "A média das idades"
from	aluno
group by uf
having avg(extract(year from current_date) - ano_nascimento) > 30;


-- Produto cartesiano
insert into professor (nome)
values 
('Claudio'),
('Fernando'),
('Mario'),
('Osvaldo'),
('Arnaldo');

insert into disciplina (codigo, nome, matricula_professor)
values 
('BES022','Teste e Qualidade', (select matricula from professor where nome='Claudio')),
('BES027','Bando de Dados 2', (select matricula from professor where nome='Claudio')),
('BES008','POO', (select matricula from professor where nome='Claudio')),
('BES003','LPA', (select matricula from professor where nome='Fernando')),
('BES035','Programação WEB', (select matricula from professor where nome='Mario' )),
('BES040','Programação Mobile', (select matricula from professor where nome='Mario'));

select *
from disciplina;

select *
from professor;

select *
from aluno;

-- NÃO É PRA FAZER ASSIM!
select	*
from	disciplina d,
		professor p
where	d.matricula_professor = p.matricula
and		p.nome = 'Claudio';
		
select	*
from	disciplina d
		inner join professor p on (p.matricula = d.matricula_professor)
where	p.nome = 'Claudio';
		
-- Quais as disciplinas e seus respectivos professores 		
-- Saída: codigo e nome da disciplina e nome do professor
select	d.codigo as "Código da disciplina",
		d.nome as "Nome da disciplina",
		p.nome as "Nome do professor"
from	disciplina d
		inner join professor p on (p.matricula = d.matricula_professor);
		
-- Quais os professores e suas respectivas disciplinas, professores que não lecionam nenhuma disciplina
-- devem ser retornados e, no lugar do nome da disciplina deve ser indicado '(Não leciona nenhuma disciplina)'
-- Saída: nome do professor,  codigo e nome da disciplina
-- A saída deve ser ordenada por nome da disciplina
select	*
from	professor p
		left outer join disciplina d on (d.matricula_professor = p.matricula);

select 	p.nome as "Nome do professor",
		d.codigo as "Código da disciplina",
		d.nome as "Nome da disciplina"
from	professor p
		left outer join disciplina d on (d.matricula_professor = p.matricula);

select 	p.nome as "Nome do professor",
		d.codigo as "Código da disciplina",
		coalesce(d.nome, '(Não leciona nenhuma disciplina)') as "Nome da disciplina"
from	professor p
		left outer join disciplina d on (d.matricula_professor = p.matricula);

select 	p.nome as "Nome do professor",
		d.codigo as "Código da disciplina",
		coalesce(d.nome, '(Não leciona nenhuma disciplina)') as "Nome da disciplina"
from	professor p
		left outer join disciplina d on (d.matricula_professor = p.matricula)
order by d.nome asc;


-- Poderíamos utilizar o right outer join também
select	*
from	disciplina d
		right outer join professor p on (p.matricula = d.matricula_professor);
		
-- Lembando como ocorre o produto cartesiano antes da filtragem da junção		
select * 
from professor, disciplina	
	where professor.nome = 'Osvaldo';		

-- Quais as combinações possíveis de todos os professores com todas as disciplinas?
-- Saída: nome do professor e o nome da disciplina
select	*
from	professor p
		cross join disciplina d;

select	p.nome as "Nome do professor",
		d.nome as "Nome da disciplina"
from	professor p
		cross join disciplina d;

-- Acrescentar ao professor a disciplina na qual ele é titular (regra: um professor só pode ser titular de uma
-- única disciplina)
--alter table professor
--	add column cod_disciplina_titula char(6);
-- alter table professor 
--	rename column cod_disciplina_titula to cod_disciplina_titular;

alter table professor
	add column cod_disciplina_titular char(6);

select * 
from professor;

update professor
set	cod_disciplina_titular = 'BES008'
where nome = 'Claudio';

update professor
set	cod_disciplina_titular = 'BES040'
where nome = 'Mario';

-- Quais são os professores e as respectivas disciplinas na qual é titular. Caso uma disciplina não
-- tenha nenhum professor como titular, deve aparecer a mensagem '(Sem professor titular)'
-- Caso um professor não seja titular em nenhuma disciplina , deve aparecer a mensagem '(Não é titular 
-- de nenhuma disciplina)'
-- Ou seja, você deve retornar todas as disciplinas e todos os professores, mas associar as disciplinas
-- aos respectivos professores titulares quando for o caso.

select	*
from	disciplina d
		inner join professor p on (p.cod_disciplina_titular = d.codigo); -- NÃO RESOLVE

select	*
from	disciplina d
		left outer join professor p on (p.cod_disciplina_titular = d.codigo); -- NÃO RESOLVE

select	*
from	disciplina d
		right outer join professor p on (p.cod_disciplina_titular = d.codigo); -- NÃO RESOLVE

select	*
from	disciplina d
		full outer join professor p on (p.cod_disciplina_titular = d.codigo); -- NÃO RESOLVE

-- Natural join
select *
from professor;

alter table professor
	rename column matricula to matricula_professor;
alter table professor
	rename column nome to nome_professor;

select *
from disciplina;

alter table disciplina
	rename column nome to nome_disciplina ;
	
alter table disciplina
	rename column codigo to codigo_disciplina;

select *
from	disciplina d
		natural join professor p;
		
select	*
from	professor p
		left outer join disciplina d on (d.matricula_professor = p.matricula_professor);
select	*
from	professor p
		left outer join disciplina d using (matricula_professor);


		