-- trigger: v1.0
CREATE OR REPLACE FUNCTION f_trg_ins_movimento()
RETURNS TRIGGER
AS
$$
DECLARE
	v_qtd_saldo_anterior		integer;
	v_data_saldo_anterior		date;
BEGIN

	IF EXISTS (SELECT * FROM saldo WHERE id_item = NEW.id_item AND data = NEW.data) THEN
		
		UPDATE	saldo
		SET 	qtd = qtd + NEW.qtd
		WHERE	id_item = NEW.id_item
		AND		data   >= NEW.data;		
		
	ELSE
	
		IF EXISTS (SELECT * FROM saldo WHERE id_item = NEW.id_item AND data < NEW.data) THEN
		
			SELECT	max(data)
			INTO	v_data_saldo_anterior
			FROM	saldo
			WHERE	id_item	= NEW.id_item
			AND		data	< NEW.data;
					
			SELECT	qtd
			INTO	v_qtd_saldo_anterior
			FROM	saldo
			WHERE	id_item	= NEW.id_item
			AND		data	= v_data_saldo_anterior;

		END IF;
		
		INSERT INTO saldo (data, id_item, qtd)
			VALUES (NEW.data, NEW.id_item, COALESCE(v_qtd_saldo_anterior, 0) + NEW.qtd);
			
		UPDATE	saldo
		SET 	qtd = qtd + NEW.qtd
		WHERE	id_item = NEW.id_item
		AND		data	> NEW.data;		
			
	END IF;
	
	RETURN NEW;
END;
$$
LANGUAGE PLPGSQL;


-- trigger: v2.0
CREATE OR REPLACE FUNCTION f_trg_ins_movimento()
RETURNS TRIGGER
AS
$$
DECLARE
	v_qtd_saldo_anterior		integer;
	v_data_saldo_anterior		date;
BEGIN

	-- se não existir saldo na data , então, vamos criar um saldo na data, com qtd ZERO ou com qtd ANTERIOR (caso exista).
	IF NOT EXISTS (SELECT * FROM saldo WHERE id_item = NEW.id_item AND data = NEW.data) THEN
	
		SELECT	max(data)
		INTO	v_data_saldo_anterior
		FROM	saldo
		WHERE	id_item	= NEW.id_item
		AND		data	< NEW.data;

		SELECT	qtd
		INTO	v_qtd_saldo_anterior
		FROM	saldo
		WHERE	id_item	= NEW.id_item
		AND		data	= v_data_saldo_anterior;
		
		INSERT INTO saldo (data, id_item, qtd)
			VALUES (NEW.data, NEW.id_item, COALESCE(v_qtd_saldo_anterior, 0));
	END IF;

	-- atualizar o saldo na data e datas posteriores.
	UPDATE	saldo
	SET 	qtd = qtd + NEW.qtd
	WHERE	id_item = NEW.id_item
	AND		data   >= NEW.data;		

	RETURN NEW;
END;
$$
LANGUAGE PLPGSQL;

-- trigger: v3.0
-- função para obtenção do saldo anterior (NÃO é uma função diretamente relacionada à triggers)

DROP FUNCTION IF EXISTS f_saldo_anterior;

CREATE OR REPLACE FUNCTION f_saldo_item(p_id_item INTEGER, p_data DATE)
RETURNS INTEGER
AS
$$
DECLARE
	v_qtd_saldo_anterior		INTEGER;
	v_data_saldo_anterior		DATE;
BEGIN
	SELECT	max(data)
	INTO	v_data_saldo_anterior
	FROM	saldo
	WHERE	id_item	= p_id_item
	AND		data   <= p_data;

	SELECT	qtd
	INTO	v_qtd_saldo_anterior
	FROM	saldo
	WHERE	id_item	= p_id_item
	AND		data	= v_data_saldo_anterior;

	RETURN COALESCE(v_qtd_saldo_anterior,0);
END;
$$
LANGUAGE PLPGSQL;

-- Exemplo de chamada da função de cálculo de saldo anterior
select f_saldo_item(2, '2021-03-28'); -- esperado = 37
select f_saldo_item(2, '2021-04-02'); -- esperado = 63
select f_saldo_item(2, '2021-06-21'); -- esperado = 77
select f_saldo_item(2, '2021-06-20'); -- esperado = 63
-- 2021-02-10, 2, 3
-- 2021-03-01, 2, 37
-- 2021-03-02, 1, 10
-- 2021-03-05, 3, 8
-- 2021-04-02, 2, 63
-- 2021-06-21, 2, 77

CREATE OR REPLACE FUNCTION f_trg_ins_movimento()
RETURNS TRIGGER
AS
$$
DECLARE
	v_qtd_saldo_anterior		INTEGER;
BEGIN

	-- se não existir saldo na data , então, vamos criar um saldo na data, com qtd ZERO ou com qtd ANTERIOR (caso exista).
	IF NOT EXISTS (SELECT * FROM saldo WHERE id_item = NEW.id_item AND data = NEW.data) THEN

		v_qtd_saldo_anterior := f_saldo_item(NEW.id_item, NEW.data - 1);

		INSERT INTO saldo (data, id_item, qtd)
			VALUES (NEW.data, NEW.id_item, v_qtd_saldo_anterior);
			
	END IF;

	-- atualizar o saldo na data e datas posteriores.
	UPDATE	saldo
	SET 	qtd = qtd + NEW.qtd
	WHERE	id_item = NEW.id_item
	AND		data   >= NEW.data;		

	-- Não deve ser permitido estoque negativo para um item.
	-- Essa validação poderia ocorrer numa constraint de check, mas ficando no trigger vc tem maior flexibilidade.
	-- Por exemplo, vc poderia configurar a qtd mínima para cada item (criando um atributo qtd_minima na tabela de item)
	-- e utilizando esse dado no trigger. Lembrando que a constriaint de check não permite uso de dados de outra tupla
	-- e/ou outra tabela.
	IF EXISTS (SELECT * from saldo WHERE id_item = NEW.id_item AND data >= NEW.data AND qtd < 0) THEN
		RAISE EXCEPTION 'Quantidade insuficiente de estoque!';
	END IF;

	RETURN NEW;
END;
$$
LANGUAGE PLPGSQL;


CREATE TRIGGER trg_ins_movimento
BEFORE INSERT
ON movimento
FOR EACH ROW 
EXECUTE PROCEDURE f_trg_ins_movimento();

INSERT INTO item (nome)
VALUES 
('Parafuso'),
('Porca'),
('Vela'),
('Bobina');

select *
from item;

delete 
from movimento;
delete
from saldo;

select *
from movimento;

select	*
from	saldo
order by data asc,
		 id_item asc;

-- 1. 1º movimento para cada item no estoque - k
--	  inserir (insert) um saldo na data, com a qtd igual a do movimento
INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-03-01', (select id from item where nome = 'Porca'), 30);
INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-03-02', (select id from item where nome = 'Parafuso'), 10);
INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-03-05', (select id from item where nome = 'Vela'), 8);
-- saldo: 
-- 2021-03-01, 2, 30
-- 2021-03-02, 1, 10
-- 2021-03-05, 3, 8

-- 2. novo movimento para um item na mesma data de outro movimento para o mesmo item
--    novo movimento para um item numa data onde já existe saldo para o mesmo <<--
--	  atualização (update) do saldo naquela data
INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-03-01', (select id from item where nome = 'Porca'), 4);
-- 2021-03-01, 2, 34	
-- 2021-03-02, 1, 10
-- 2021-03-05, 3, 8

-- 3. novo movimento para um item após o último movimento para o mesmo item
--    existe saldo anterior????
--	  inserir (insert) um saldo na data, considerando a qtd saldo anterior e a quantidade do movimento
INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-06-21', (select id from item where nome = 'Porca'), 14);
-- 2021-03-01, 2, 34
-- 2021-03-02, 1, 10
-- 2021-03-05, 3, 8
-- 2021-06-21, 2, 48

-- 4. novo movimento para um item antes do primeiro movimento para o mesmo item
-- 	  não existe saldo anterior, mas existem saldos posteriores
--	  inserir (insert) um saldo na data, com a qtd igual a do movimento
--	  atualizar (update) o saldo nas datas posteriores
INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-02-10', (select id from item where nome = 'Porca'), 3);
-- 2021-02-10, 2, 3
-- 2021-03-01, 2, 37
-- 2021-03-02, 1, 10
-- 2021-03-05, 3, 8
-- 2021-06-21, 2, 51

-- 5. novo movimento para um item com data entre outros movimentos para o mesmo item
--	  inserir (insert) um saldo na data, considerando a qtd saldo anterior e a quantidade do movimento
--	  atualizar (update) o saldo nas datas posteriores
INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-04-02', (select id from item where nome = 'Porca'), 6);
-- 2021-02-10, 2, 3
-- 2021-03-01, 2, 37
-- 2021-03-02, 1, 10
-- 2021-03-05, 3, 8
-- 2021-04-02, 2, 43
-- 2021-06-21, 2, 57

-- 6. novo movimento para um item na mesma data de outro movimento, mas com movimentos posteriores existentes
--    existe saldo na data e em datas postariores
--	  atualizar (update) o saldo na data do movimento e nas datas posteriores
INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-04-02', (select id from item where nome = 'Porca'), 20);
-- 2021-02-10, 2, 3
-- 2021-03-01, 2, 37
-- 2021-03-02, 1, 10
-- 2021-03-05, 3, 8
-- 2021-04-02, 2, 63
-- 2021-06-21, 2, 77

-- 7. movimento de saída de estoque válido (com quantidade suficiente no estoque)
INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-03-10', (select id from item where nome = 'Porca'), -5);
-- 2021-02-10, 2, 3
-- 2021-03-01, 2, 37
-- 2021-03-02, 1, 10
-- 2021-03-05, 3, 8
-- 2021-03-10, 2, 32
-- 2021-04-02, 2, 58
-- 2021-06-21, 2, 72

-- 7. movimento de saída de estoque NÃO válido (com quantidade INSUFICIENTE no estoque)
INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-03-12', (select id from item where nome = 'Porca'), -50);
-- espero uma mensagem de falhar e o cancelamento da operação
-- 2021-02-10, 2, 3
-- 2021-03-01, 2, 37
-- 2021-03-02, 1, 10
-- 2021-03-05, 3, 8
-- 2021-03-10, 2, 32
-- 2021-04-02, 2, 58
-- 2021-06-21, 2, 72




select *
from movimento;

select *
from saldo;



