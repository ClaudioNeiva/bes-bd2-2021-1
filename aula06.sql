-- Atributos derivados
-- tabelas: nf, item_nf
-- item_nf: qtd, valor_unitario
-- v_item_nf: qtd, valor_unitario, (qtd * valor_unitario) as "valor_total"
-- select qtd, valor_unitario, f_calc_total(qtd, valor_unitario) from....

-- item_nf: qtd, valor_unitario, valor_total
-- 10, 5, 40????? insert -> rejeitada
-- 10, 5, 50 - insert ok
-- 8, 5, 50??????? update (qtd de 10 para 8) -> rejeitado
-- Trigger (ESTÁ DA TRANSAÇÃO E É SEMPRE (CONFIGURAÇÃO) EXECUTADO)
-- insert into (qtd, valor_unitario, valor_total XXXXXXX)
-- insert into (qtd, valor_unitario) -> o trigger vai gerar e atualizar o valor total
-- update item_nf set valor_total = XXXXXXX -> o trigger vai atualizar o valor total a partir
-- da atualização da qtd e/ou valor_unitario
-- update, delete, insert
-- update, insert -> disparo do trigger -> calcular e armazenar o valor total a partir da qtd e valor unitário

-- Validações
-- Um médico não pode marcar mais de 3 consultas para uma 1 hora
-- hr_inicio e hr_fim
-- Reg1: 10:00 - 10:30
-- Reg2:    10:10 - 10:40
-- Trigger
-- update, delete, insert

-- Auditoria (armazenamento de dados de auditoria)
-- Problema: nem sempre o usuário no banco é o usuário real (por exemplo, sistema WEB)


-- CENÁRIO PARA USO DE TRIGGERS

-- Controle de estoque

create table item(
	id			serial			not null,
	nome		varchar(30)		not null,
	constraint pk_item
		primary key (id));
	
create table movimento(
	id			serial			not null,
	data		date			not null,
	id_item		integer			not null,
	qtd			integer			not null,
	constraint pk_movimento
		primary key (id));	
		
create table saldo(
	data		date			not null,
	id_item		integer			not null,
	qtd			integer			not null,
	constraint pk_saldo
		primary key (data, id_item));	
		
alter table movimento
	add constraint fk_movimento_item
		foreign key (id_item)
		references item;
alter table saldo
	add constraint fk_saldo_item
		foreign key (id_item)
		references item;		
		
-- Criar um trigger apenas para verificar seu disparo

-- estado A    (4 registros)
-- insert REG1
-- BEFORE: executa um programa, pode modificar o REG1 antes do mesmo ser inserido na base
-- o registro é inserido na base 
-- estado B 	(5 registros)

-- estado A    (4 registros)
-- insert REG1
-- o registro é inserido na base 
-- estado B 	(5 registros)
-- AFTER: executa um programa, NÃO pode modificar o REG1 pois o mesmo já foi inserido na base
-- Como o trigger está no contexto da transação, ele ainda pode fazer o cancelamento da mesma.

-- tabela de movimento_importado
-- insert into movimento 
--     select * from movimento_importado
-- insert into movimento (campo1, campo2, campo3) 
--     select campo1, campo2, campo3 from movimento_importado
-- update/delete podem afetar diversas linhas do seu banco

CREATE OR REPLACE FUNCTION f_trg_ins_movimento()
RETURNS TRIGGER
AS
$$
BEGIN
	RAISE NOTICE 'Oi... executei o trigger para os seguintes dados: id=%, data=%, id_item=%, qtd=% ', NEW.id, NEW.data, NEW.id_item, NEW.qtd;
	RETURN NEW;
END;
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER trg_ins_movimento
BEFORE INSERT
ON movimento
FOR EACH ROW 
EXECUTE PROCEDURE f_trg_ins_movimento();

INSERT INTO item (nome)
VALUES 
('Parafuso'),
('Porca'),
('Vela'),
('Bobina');

select *
from item;

INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-03-01', (select id from item where nome = 'Porca'), 30);

INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-03-02', (select id from item where nome = 'Parafuso'), 10);

INSERT INTO movimento (data, id_item, qtd)
VALUES ('2021-03-05', (select id from item where nome = 'Vela'), 8);

select *
from movimento;









