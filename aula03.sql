-- Inserção
insert into aluno (nome, cpf)
values 
('Claudio Neiva', '123'),
('Antonio', '234'),
('Maria', '345'),
('Pedro', '456')
;

select *
from aluno;

insert into aluno_telefone (matricula,telefone)
values
((select matricula from aluno where nome = 'Claudio Neiva'),'3563546653'),
((select matricula from aluno where nome = 'Claudio Neiva'),'5678567567'),
((select matricula from aluno where nome = 'Maria'),'6578678884');

select *
from aluno_telefone;

-- Alteração
update 	aluno
	set	nome = 'Antonio Neiva',
		cpf = '78978'
where	nome = 'Antonio';

select *
from aluno;

-- Deleção
delete
from	aluno
where	nome = 'Antonio Neiva';

select *
from aluno;

select *
from aluno_telefone;

delete
from 	aluno
where	nome = 'Maria';

select *
from aluno;

select *
from aluno_telefone;

-- Consulta
select 	* 
from	aluno;

select 	nome, cpf
from 	aluno;

select	*
from	aluno
where	cpf = '123';

select 	nome, cpf
from 	aluno
where	cpf = '123';

select	a.matricula,
		a.nome,
		a.cpf,
		at.telefone
from 	aluno a
	inner join aluno_telefone at on (at.matricula = a.matricula);
	
-- Operadores aritméticos, relacionais e lógicos
select 	4 + 6 * 2 / 5 - 8;
select	5 > 8, 5 >= 10, 50 < 100, 45<=45, 6 <> 8; 
select	not (5 > 8) and (5 >= 10) or (50 < 100); 

-- Is null | is not null

alter table aluno
	add column email varchar(255);

insert into aluno (nome, cpf)
values 
('Antonio', '234'),
('Maria', '345')
;

select *
from aluno;

update	aluno
	set	email = nome || '@ucsal.br'
where	nome in ('Antonio', 'Maria');

select *
from aluno;

select	*
from 	aluno
where	email is null;

select	*
from 	aluno
where	email is not null;

-- Coalesce
select 	matricula,
		nome,
		cpf,
		coalesce(email, '(email não cadastrado)')
from aluno;

-- Operadores like e ilike
select	'antonio claudio' like 'antonio%';

insert into aluno (nome, cpf)
values 
('José Claudio','765'),
('Antonio Claudio Neiva', '987');

select	*
from 	aluno;

select	*
from	aluno
where	nome like 'Claudio%';

select	*
from	aluno
where	nome like '%Claudio';

select	*
from	aluno
where	nome like '%Claudio%';

select	*
from	aluno
where	nome ilike '%claudio%';

select	*
from	aluno
where	lower(nome) like '%claudio%';

select	*
from	aluno
where	upper(nome) like '%CLAUDIO%';

select	*
from	aluno
where	nome like '_a%';

-- Funções de string: ltrim, rtrim, trim, concat, lower, upper, substr, length e position
select	'*' || ltrim('   claudio   ') || '*',
		'*' || rtrim('   claudio   ') || '*',
		'*' || trim('   claudio   ') || '*',
		'*' || '   claudio   ' || '*';
		
insert into aluno (nome, cpf)
values
('   claudio   ', '639');

select	trim(nome),
		nome 
from 	aluno;

select	*
from 	aluno
where	trim(nome) ='claudio';

select	concat('antonio ','claudio ','neiva');

select	concat('nome=', nome, ', email=', coalesce(email,'(email não cadastrado)'))
from	aluno;

select	nome,
		lower(nome),
		upper(nome)
from	aluno;		

select	substring(nome,1,3),
		substr(nome,3,2),
		substr(nome,5)
from	aluno;

select	nome,
		length(nome) as "Qtd de caracteres"
from	aluno;

select	nome,
		position('au' in nome) as "Posição de 'au' no nome"
from	aluno;

-- Distinct
select	*
from	aluno;

insert into aluno_telefone (matricula,telefone)
values
((select matricula from aluno where nome = 'Maria'),'545454555'),
((select matricula from aluno where nome = 'Antonio'),'545454555'),
((select matricula from aluno where nome = 'Pedro'),'3563546653');

select *
from aluno_telefone;

select	distinct telefone
from	aluno_telefone;

-- current_date, current_time, current_timestamp
-- extract(campo from data/hora)

alter table aluno
	add	column ano_nascimento int;
	
select	*
from	aluno;

update	aluno
set		ano_nascimento = 1980
where	nome = 'Claudio Neiva';

update	aluno
set		ano_nascimento = 2001
where	nome = 'Pedro';

update	aluno
set		ano_nascimento = 1965
where	nome = 'Antonio';

update	aluno
set		ano_nascimento = 1998
where	nome = 'Maria';

update	aluno
set		ano_nascimento = 1976
where	nome = 'Antonio Claudio Neiva';

update	aluno
set		ano_nascimento = 1982
where	nome = 'José Claudio';

delete	
from	aluno
where	trim(nome) = 'claudio';

select	current_date, current_time, current_timestamp;

select	extract(year from current_date);

select	nome,
		extract(year from current_date) - ano_nascimento as "Idade"
from	aluno;

-- Funções de agregação: count, max, min, sum, avg

select	count(*)		as "Qtd total alunos",
		count(email)	as "Qtd alunos com email cadastrado",
		count(*) - count(email) as "Qtd alunos sem email cadastrado",
		min(extract(year from current_date) - ano_nascimento)	as "A idade domais jovem", 
		max(extract(year from current_date) - ano_nascimento)	as "A idade do mais velho",
		sum(extract(year from current_date) - ano_nascimento)	as "Soma das idades",
		avg(extract(year from current_date) - ano_nascimento) 	as "A média das idades"
from	aluno;

-- Adicionar a UF do aluno

alter table aluno
	add	column uf char(2);

update	aluno
set		uf = 'BA'
where	nome = 'Claudio Neiva';

update	aluno
set		uf = 'SP'
where	nome = 'Pedro';

update	aluno
set		uf = 'BA'
where	nome = 'Antonio';

update	aluno
set		uf = 'RJ'
where	nome = 'Maria';

update	aluno
set		uf = 'RJ'
where	nome = 'Antonio Claudio Neiva';

update	aluno
set		uf = 'BA'
where	nome = 'José Claudio';

-- Group by

select *
from aluno;

select	count(*) as "Qtd alunos",
		avg(extract(year from current_date) - ano_nascimento) 	as "A média das idades",
		uf
from	aluno
group by uf;

-- 3	"Claudio Neiva"	"123        "		1980	"BA"
-- 17	"Antonio"	"234        "	"Antonio@ucsal.br"	1965	"BA"
-- 20	"José Claudio"	"765        "		1982	"BA"
-- tupla no resultado:	3	45.3333333333333	"BA"

-- 18	"Maria"	"345        "	"Maria@ucsal.br"	1998	"RJ"
-- 19	"Antonio Claudio Neiva"	"987        "		1976	"RJ"
-- tupla no resultado:	2	34	"RJ"

-- 6	"Pedro"	"456        "		2001	"SP"
-- tupla no resultado:	1	20	"SP"

-- Order by
select	*
from	aluno
order by nome asc;

select	*
from	aluno
order by nome desc;

select	*
from	aluno
order by uf asc,
		 nome asc;

select	count(*) as "Qtd alunos",
		avg(extract(year from current_date) - ano_nascimento) 	as "A média das idades",
		uf
from	aluno
group by uf
order by 2 desc;

-- Cast ou operador de conversão de tipos (::)
-- suposição: dois atributos de data (data da compra e data da venda) from armazenados como string (char(10)),
-- como calcular a quantidade de dias entre a compra e a venda?
select cast('2018-05-07' as date) - cast('2015-05-07' as date);
select '2018-05-07'::date - '2015-05-07'::date;

select	cast('2021' as integer) - cast('1985' as integer);
select	'2021'::integer - '1985'::integer;








