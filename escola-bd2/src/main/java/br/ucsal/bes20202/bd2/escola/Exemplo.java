package br.ucsal.bes20202.bd2.escola;

import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		Aluno aluno = new Aluno();
		aluno.setNome("Claudio");
		aluno.setTelefones(Arrays.asList("12312312","543645645","56875667"));
		aluno.setSituacao(SituacaoAlunoEnum.FORMADO);
		
		em.getTransaction().begin();
		em.persist(aluno);
		em.getTransaction().commit();
//		
//		em.getTransaction().begin();
//		em.remove(aluno);
//		em.getTransaction().commit();

		emf.close();

	}

}
