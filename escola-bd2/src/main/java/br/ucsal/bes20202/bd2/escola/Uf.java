package br.ucsal.bes20202.bd2.escola;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_uf")
public class Uf implements Serializable {

	private static final long serialVersionUID = 1L;

	// char(2)
	@Id
	@Column(columnDefinition = "char(2)")
	private String sigla;

	// varchar(40)
	@Column(length = 40)
	private String nome;

	// @OneToMany(mappedBy = "uf", cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, fetch = FetchType.EAGER)
	// @OneToMany(mappedBy = "uf", cascade = { CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.DETACH })
	// @OneToMany(mappedBy = "uf", cascade = { CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE })
	@OneToMany(mappedBy = "uf", cascade = { CascadeType.ALL }, orphanRemoval = true)
	private List<Cidade> cidades;

	public Uf() {
	}

	public Uf(String sigla, String nome) {
		super();
		this.sigla = sigla;
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((sigla == null) ? 0 : sigla.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Uf other = (Uf) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (sigla == null) {
			if (other.sigla != null)
				return false;
		} else if (!sigla.equals(other.sigla))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Uf [sigla=" + sigla + ", nome=" + nome + ", cidades=" + cidades + "]";
	}

}
