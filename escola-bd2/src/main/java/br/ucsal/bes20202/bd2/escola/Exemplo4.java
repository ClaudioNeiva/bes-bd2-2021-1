package br.ucsal.bes20202.bd2.escola;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo4 {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		Uf ufSP = em.find(Uf.class, "SP");

		System.out.println("ufSP.nome=" + ufSP.getNome());
		ufSP.getCidades().size();

		emf.close();
		
		System.out.println("ufSP.cidades=" + ufSP.getCidades());
		
	}

}
