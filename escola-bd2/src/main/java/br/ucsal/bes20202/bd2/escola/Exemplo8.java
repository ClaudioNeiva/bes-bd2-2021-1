package br.ucsal.bes20202.bd2.escola;

import java.io.IOException;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo8 {

	public static void main(String[] args) throws ClassNotFoundException, IOException {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		AlunoExterno alunoExterno = new AlunoExterno("Joana Externo", SituacaoAlunoEnum.ATIVO, "Maristas");
		Aluno aluno = new Aluno("Pedro Normal", SituacaoAlunoEnum.ATIVO);

		em.getTransaction().begin();
		em.persist(aluno);
		em.persist(alunoExterno);
		em.getTransaction().commit();

		new Scanner(System.in).nextLine();
		
		emf.close();

	}

}
