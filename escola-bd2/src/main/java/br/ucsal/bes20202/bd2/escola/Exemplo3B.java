package br.ucsal.bes20202.bd2.escola;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo3B {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		Cidade cidadeSSA = em.find(Cidade.class, "SSA");		
		
		Uf ufSP = em.find(Uf.class, "SP");		

		em.getTransaction().begin();

		cidadeSSA.setUfParceira(ufSP);

		em.getTransaction().commit();

		emf.close();

	}

}
