package br.ucsal.bes20202.bd2.escola;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

// tab_aluno
@Entity
@Table(name = "tab_aluno", uniqueConstraints = {
		@UniqueConstraint(name = "uk_aluno_rg", columnNames = { "num_rg", "orgao_expedidor", "uf_orgao_expedidor_sigla" }),
		@UniqueConstraint(name = "uk_aluno_cpf", columnNames = { "cpf" }) })
@SequenceGenerator(name = "sq_aluno", sequenceName = "seq_aluno")
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@DiscriminatorColumn(name = "tipo", length = 3)
//@DiscriminatorValue(value = "ALN")
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQuery(query = "select a from Aluno a where a.situacao = :situacao order by a.nome", name = "alunos por situação")
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
public class Aluno {

	// int - pk
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_aluno")
	private Integer matricula;

	// varchar(40)
	@Column(length = 40)
	private String nome;

	// char(11) - unique1
	@Column(columnDefinition = "char(11)")
	private String cpf;

//	@ManyToMany
//	@JoinTable(
//	name = "tab_aluno_curso", 
//	joinColumns = { @JoinColumn(name = "matricula_aluno") }, 
//	inverseJoinColumns = { @JoinColumn(name = "codigo_curso")}
//	)
//	private List<Curso> cursos;
	@ManyToOne
	private Curso curso;

	// char(3)
	// @Convert(converter = SituacaoAlunoConverter.class)
	// @Column(columnDefinition = "char(3)")
	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = "situacao_aluno_t")
	@Type(type = "pgsql_enum")
	private SituacaoAlunoEnum situacao;

	// numeric(10,2) - renda_familiar
	@Column(name = "renda_familiar", precision = 10, scale = 2)
	private BigDecimal rendaFamiliar;

	// unique2
	// num_rg
	@Column(name = "num_rg")
	private Long numRg;

	// unique2
	// orgao_expedidor
	@Column(name = "orgao_expedidor")
	private String orgaoExpedidor;

	// unique2
	@ManyToOne()
	@JoinColumn(name = "uf_orgao_expedidor_sigla")
	private Uf ufOrgaoExpedidor;

	// cada telefone deve ser varchar(15)
	@ElementCollection
	@Column(name = "telefone", length = 15)
	private List<String> telefones;

	@Embedded
	private Endereco endereco;

	public Aluno() {
		super();
	}

	public Aluno(String nome, SituacaoAlunoEnum situacaoAlunoEnum) {
		this.nome = nome;
		this.situacao = situacaoAlunoEnum;
	}

	public Aluno(String nome, Curso curso, SituacaoAlunoEnum situacao) {
		this(nome, situacao);
		this.curso = curso;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public SituacaoAlunoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAlunoEnum situacao) {
		this.situacao = situacao;
	}

	public BigDecimal getRendaFamiliar() {
		return rendaFamiliar;
	}

	public void setRendaFamiliar(BigDecimal rendaFamiliar) {
		this.rendaFamiliar = rendaFamiliar;
	}

	public Long getNumRg() {
		return numRg;
	}

	public void setNumRg(Long numRg) {
		this.numRg = numRg;
	}

	public String getOrgaoExpedidor() {
		return orgaoExpedidor;
	}

	public void setOrgaoExpedidor(String orgaoExpedidor) {
		this.orgaoExpedidor = orgaoExpedidor;
	}

	public Uf getUfOrgaoExpedidor() {
		return ufOrgaoExpedidor;
	}

	public void setUfOrgaoExpedidor(Uf ufOrgaoExpedidor) {
		this.ufOrgaoExpedidor = ufOrgaoExpedidor;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((curso == null) ? 0 : curso.hashCode());
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((numRg == null) ? 0 : numRg.hashCode());
		result = prime * result + ((orgaoExpedidor == null) ? 0 : orgaoExpedidor.hashCode());
		result = prime * result + ((rendaFamiliar == null) ? 0 : rendaFamiliar.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		result = prime * result + ((ufOrgaoExpedidor == null) ? 0 : ufOrgaoExpedidor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (curso == null) {
			if (other.curso != null)
				return false;
		} else if (!curso.equals(other.curso))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (numRg == null) {
			if (other.numRg != null)
				return false;
		} else if (!numRg.equals(other.numRg))
			return false;
		if (orgaoExpedidor == null) {
			if (other.orgaoExpedidor != null)
				return false;
		} else if (!orgaoExpedidor.equals(other.orgaoExpedidor))
			return false;
		if (rendaFamiliar == null) {
			if (other.rendaFamiliar != null)
				return false;
		} else if (!rendaFamiliar.equals(other.rendaFamiliar))
			return false;
		if (situacao != other.situacao)
			return false;
		if (telefones == null) {
			if (other.telefones != null)
				return false;
		} else if (!telefones.equals(other.telefones))
			return false;
		if (ufOrgaoExpedidor == null) {
			if (other.ufOrgaoExpedidor != null)
				return false;
		} else if (!ufOrgaoExpedidor.equals(other.ufOrgaoExpedidor))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", cpf=" + cpf + ", curso=" + curso + ", situacao=" + situacao
				+ ", rendaFamiliar=" + rendaFamiliar + ", numRg=" + numRg + ", orgaoExpedidor=" + orgaoExpedidor + ", ufOrgaoExpedidor="
				+ ufOrgaoExpedidor + ", telefones=" + telefones + ", endereco=" + endereco + "]";
	}

}
