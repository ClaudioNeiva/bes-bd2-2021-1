package br.ucsal.bes20202.bd2.escola;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo3 {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Uf ufSP = new Uf("SP", "São Paulo");

		List<Cidade> cidades = new ArrayList<>();
		cidades.add(new Cidade(ufSP, "SAO", "São Paulo"));
		cidades.add(new Cidade(ufSP, "GRU", "Guarulhos"));
		cidades.add(new Cidade(ufSP, "SBC", "São Bernardo do Campo"));

		ufSP.setCidades(cidades);

		em.persist(ufSP);

		em.getTransaction().commit();

		emf.close();

	}

}
