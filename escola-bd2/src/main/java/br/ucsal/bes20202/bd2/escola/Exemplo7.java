package br.ucsal.bes20202.bd2.escola;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo7 {

	public static void main(String[] args) throws ClassNotFoundException, IOException {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		Uf ufBA = em.find(Uf.class, "BA");
		System.out.println("ufBA.cidades=" + ufBA.getCidades());
		ufBA = fullDetachUf(ufBA);

		ufBA.setNome("Nova Bahia 9");
		ufBA.getCidades().get(2).getUfParceira().setNome("São Paulo 5");

		em.getTransaction().begin();
		// Manipulando Curso!!!
		em.getTransaction().commit();

		emf.close();

	}

	private static Uf fullDetachUf(Uf uf) throws IOException, ClassNotFoundException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(uf);
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bais);
		return (Uf) ois.readObject();
	}

}
