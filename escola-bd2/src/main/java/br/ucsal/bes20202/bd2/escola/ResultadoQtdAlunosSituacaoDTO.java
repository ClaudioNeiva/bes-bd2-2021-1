package br.ucsal.bes20202.bd2.escola;

public class ResultadoQtdAlunosSituacaoDTO {

	private SituacaoAlunoEnum situacaoAluno;

	private Long qtd;

	public ResultadoQtdAlunosSituacaoDTO(SituacaoAlunoEnum situacaoAluno, Long qtd) {
		super();
		this.situacaoAluno = situacaoAluno;
		this.qtd = qtd;
	}

	public SituacaoAlunoEnum getSituacaoAluno() {
		return situacaoAluno;
	}

	public void setSituacaoAluno(SituacaoAlunoEnum situacaoAluno) {
		this.situacaoAluno = situacaoAluno;
	}

	public Long getQtd() {
		return qtd;
	}

	public void setQtd(Long qtd) {
		this.qtd = qtd;
	}

	@Override
	public String toString() {
		return "ResultadoQtdAlunosSituacaoDTO [situacaoAluno=" + situacaoAluno + ", qtd=" + qtd + "]";
	}

}
