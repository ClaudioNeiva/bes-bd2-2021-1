package br.ucsal.bes20202.bd2.escola;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo5 {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		Cidade cidadeGRU = em.find(Cidade.class, "GRU");

		System.out.println("cidadeGRU.nome=" + cidadeGRU.getNome());

		emf.close();

	}

}
