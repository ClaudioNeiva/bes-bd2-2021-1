package br.ucsal.bes20202.bd2.escola;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo6 {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		Uf ufBA = em.find(Uf.class, "BA");
		System.out.println("ufBA.cidades=" + ufBA.getCidades());
		em.detach(ufBA);
		
		ufBA.setNome("Nova Bahia 8");
		ufBA.getCidades().get(2).getUfParceira().setNome("São Paulo 3");
		
		em.getTransaction().begin();
		// Manipulando Curso!!!
		em.getTransaction().commit();

		emf.close();

	}

}
