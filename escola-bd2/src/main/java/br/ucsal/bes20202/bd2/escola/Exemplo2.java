package br.ucsal.bes20202.bd2.escola;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo2 {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Uf ufSP = new Uf("SP", "São Paulo");
		// em.persist(ufSP);

		Cidade cidadeSAO = new Cidade(ufSP, "SAO", "São Paulo");
		Cidade cidadeGRU = new Cidade(ufSP, "GRU", "Guarulhos");
		Cidade cidadeSBC = new Cidade(ufSP, "SBC", "São Bernardo do Campo");
		em.persist(cidadeSAO);
		em.persist(cidadeGRU);
		em.persist(cidadeSBC);
		
		em.getTransaction().commit();

		emf.close();

	}

}
