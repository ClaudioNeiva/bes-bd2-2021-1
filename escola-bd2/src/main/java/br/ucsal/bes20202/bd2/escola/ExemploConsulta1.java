package br.ucsal.bes20202.bd2.escola;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class ExemploConsulta1 {

	private static final Scanner SCANNER = new Scanner(System.in);

	public static void main(String[] args) throws ClassNotFoundException, IOException {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		carregarDados(em);

		System.out.println("Tecle ENTER...");
		SCANNER.nextLine();

		em.clear();

		// executarConsultaSlide31(em);

		// executarConsultaSlide32A(em);
		// executarConsultaSlide32B(em);
		// executarConsultaSlide32C(em);

		// executarConsultaSlide33(em);

		// executarConsultaSlide34(em);

		executarConsultaSlide35A(em);
		//executarConsultaSlide35B(em);

		System.out.println("Tecle ENTER...");
		SCANNER.nextLine();

		emf.close();

	}

	private static void executarConsultaSlide35B(EntityManager em) {
		String oql = "select a.nome, a.endereco.cidade.nome from Aluno a ";
		TypedQuery<Object[]> query = em.createQuery(oql, Object[].class);
		List<Object[]> results = query.getResultList();
		for (Object[] result : results) {
			System.out.println(result[0] + "|" + result[1]);
		}
	}

	private static void executarConsultaSlide35A(EntityManager em) {
		String sql = "select a.nome as nomeAluno, c.nome as nomeCidade from tab_aluno a "
				+ " inner join tab_cidade c on (a.cidade_sigla = c.sigla)";
		Query query = em.createNativeQuery(sql);
		List<Object[]> results = query.getResultList();
		for (Object[] result : results) {
			System.out.println(result[0] + "|" + result[1]);
		}
	}

	private static void executarConsultaSlide34(EntityManager em) {
		TypedQuery<Aluno> query = em.createNamedQuery("alunos por situação", Aluno.class);
		query.setParameter("situacao", SituacaoAlunoEnum.ATIVO);
		List<Aluno> alunos = query.getResultList();
		System.out.println("Alunos por ordem de nome:");
		alunos.stream().forEach(System.out::println);
	}

	private static void executarConsultaSlide33(EntityManager em) {
		String oql = "select u from Uf u left outer join fetch u.cidades";
		TypedQuery<Uf> query = em.createQuery(oql, Uf.class);
		List<Uf> ufs = query.getResultList();
		System.out.println("#########################################");
		em.close();
		System.out.println("ufs=" + ufs.get(0).getCidades());
	}

	private static void executarConsultaSlide32C(EntityManager em) {
		String oql = "select new " + ResultadoQtdAlunosSituacaoDTO.class.getCanonicalName()
				+ "(a.situacao, count(*)) from Aluno a group by a.situacao";
		TypedQuery<ResultadoQtdAlunosSituacaoDTO> query = em.createQuery(oql, ResultadoQtdAlunosSituacaoDTO.class);
		List<ResultadoQtdAlunosSituacaoDTO> qtdAlunosPorSituacao = query.getResultList();
		System.out.println("Quantidade de alunos por situação:");
		qtdAlunosPorSituacao.stream().forEach(System.out::println);
	}

	private static void executarConsultaSlide32B(EntityManager em) {
		String oql = "select new br.ucsal.bes20202.bd2.escola.ResultadoQtdAlunosSituacaoDTO(a.situacao, count(*)) from Aluno a group by a.situacao";
		TypedQuery<ResultadoQtdAlunosSituacaoDTO> query = em.createQuery(oql, ResultadoQtdAlunosSituacaoDTO.class);
		List<ResultadoQtdAlunosSituacaoDTO> qtdAlunosPorSituacao = query.getResultList();
		System.out.println("Quantidade de alunos por situação:");
		qtdAlunosPorSituacao.stream().forEach(System.out::println);
	}

	private static void executarConsultaSlide32A(EntityManager em) {
		String oql = "select a.situacao, count(*) from Aluno a group by a.situacao";
		TypedQuery<Object[]> query = em.createQuery(oql, Object[].class);
		List<Object[]> qtdAlunosPorSituacao = query.getResultList();
		System.out.println("Quantidade de alunos por situação:");
		for (Object[] resultado : qtdAlunosPorSituacao) {
			System.out.println(resultado[0] + " = " + resultado[1]);
		}
	}

	private static void executarConsultaSlide31(EntityManager em) {
		// String oql = "select a from Aluno a where a.endereco.bairro = :bairro and TYPE(a) = Aluno order by a.nome asc";
		String oql = "select a from Aluno a where a.endereco.bairro = :bairro order by a.nome asc";
		TypedQuery<Aluno> query = em.createQuery(oql, Aluno.class);
		query.setParameter("bairro", "Pituaçu");
		List<Aluno> alunos = query.getResultList();
		System.out.println("Alunos que moram em Pituaçu:");
		alunos.stream().forEach(System.out::println);
	}

	private static void carregarDados(EntityManager em) {
		em.getTransaction().begin();

		Uf ufSP = new Uf("SP", "São Paulo");
		em.persist(ufSP);

		Cidade cidadeSAO = new Cidade(ufSP, "SAO", "São Paulo");
		Cidade cidadeGRU = new Cidade(ufSP, "GRU", "Guarulhos");
		Cidade cidadeSBC = new Cidade(ufSP, "SBC", "São Bernardo do Campo");
		em.persist(cidadeSAO);
		em.persist(cidadeGRU);
		em.persist(cidadeSBC);

		Curso cursoBES = new Curso("BES", "Bacharelado em Engenharia de Software", LocalDate.of(2014, 01, 05));
		em.persist(cursoBES);

		AlunoExterno alunoPedro = new AlunoExterno("Pedro", SituacaoAlunoEnum.ATIVO, "UFBA");
		alunoPedro.setEndereco(new Endereco("Rua 2", "456", "Pituaçu", cidadeGRU));
		alunoPedro.setCurso(cursoBES);
		em.persist(alunoPedro);

		Aluno alunoAna = new Aluno("Ana", SituacaoAlunoEnum.ATIVO);
		alunoAna.setEndereco(new Endereco("Rua 7", "567", "Pituaçu", cidadeGRU));
		alunoAna.setCurso(cursoBES);
		em.persist(alunoAna);

		AlunoExterno alunoJoaquim = new AlunoExterno("Joaquim", SituacaoAlunoEnum.TRANCADO, "UFBA");
		alunoJoaquim.setEndereco(new Endereco("Rua 3", "867", "Brotas", cidadeSAO));
		alunoJoaquim.setCurso(cursoBES);
		em.persist(alunoJoaquim);

		em.getTransaction().commit();
	}
}
