package br.ucsal.bes20202.bd2.escola;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tab_cidade")
public class Cidade implements Serializable {

	private static final long serialVersionUID = 1L;

	// char(3)
	@Id
	@Column(columnDefinition = "char(3)")
	private String sigla;

	// varchar(40)
	@Column(length = 40)
	private String nome;

	@ManyToOne(cascade = CascadeType.PERSIST)
	private Uf uf;

	// @ManyToOne(fetch = FetchType.LAZY)
	// @ManyToOne(cascade = CascadeType.DETACH)
	@ManyToOne
	private Uf ufParceira;

	public Cidade() {
	}

	public Cidade(Uf uf, String sigla, String nome) {
		super();
		this.uf = uf;
		this.sigla = sigla;
		this.nome = nome;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public Uf getUfParceira() {
		return ufParceira;
	}

	public void setUfParceira(Uf ufParceira) {
		this.ufParceira = ufParceira;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((sigla == null) ? 0 : sigla.hashCode());
		result = prime * result + ((uf == null) ? 0 : uf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cidade other = (Cidade) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (sigla == null) {
			if (other.sigla != null)
				return false;
		} else if (!sigla.equals(other.sigla))
			return false;
		if (uf == null) {
			if (other.uf != null)
				return false;
		} else if (!uf.equals(other.uf))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cidade [uf.sigla=" + uf.getSigla() + ", sigla=" + sigla + ", nome=" + nome + "]";
	}

}