package br.ucsal.bes20202.bd2.escola;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ExemploRemoveOrphan {

	private static final Scanner SCANNER = new Scanner(System.in);

	public static void main(String[] args) throws ClassNotFoundException, IOException {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		System.out.println("Tecle ENTER...");
		SCANNER.nextLine();
		
		em.getTransaction().begin();

		Uf ufSP = new Uf("SP", "São Paulo");
		em.persist(ufSP);

		Cidade cidadeSAO = new Cidade(ufSP, "SAO", "São Paulo");
		Cidade cidadeGRU = new Cidade(ufSP, "GRU", "Guarulhos");
		Cidade cidadeSBC = new Cidade(ufSP, "SBC", "São Bernardo do Campo");
		ufSP.setCidades(new ArrayList<>());
		ufSP.getCidades().add(cidadeSAO);
		ufSP.getCidades().add(cidadeGRU);
		ufSP.getCidades().add(cidadeSBC);
//		em.persist(cidadeSAO);
//		em.persist(cidadeGRU);
//		em.persist(cidadeSBC);

		em.getTransaction().commit();

		em.clear();
		
		ufSP = em.find(Uf.class, "SP");

		System.out.println("Tecle ENTER...");
		SCANNER.nextLine();

		em.getTransaction().begin();
		ufSP.getCidades().remove(2);
		em.getTransaction().commit();

		System.out.println("Tecle ENTER...");
		SCANNER.nextLine();

		em.clear();
		
		ufSP = em.find(Uf.class, "SP");
		System.out.println(ufSP.getCidades());

		emf.close();
	}
}
