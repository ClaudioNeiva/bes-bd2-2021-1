package br.ucsal.bes20202.bd2.escola;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo3A {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escolaPU");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Uf ufBA = new Uf("BA", "Bahia");

		List<Cidade> cidades = new ArrayList<>();
		cidades.add(new Cidade(ufBA, "SSA", "Salvador"));
		cidades.add(new Cidade(ufBA, "FDS", "Feira de Santana"));
		cidades.add(new Cidade(ufBA, "ALG", "Alagoinhas"));

		ufBA.setCidades(cidades);

		em.persist(ufBA);

		em.getTransaction().commit();

		emf.close();

	}

}
